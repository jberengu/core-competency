drop database if exists core;
create database core;
\c core

drop role if exists coremanager;
create user coremanager with password 'g~N]mD:>(9y2A,5S';

drop table if exists resturants;
drop table if exists reviews;
drop table if exists types;

create table restaurants(
	restaurantid serial PRIMARY KEY,
	name text,
	city text,
	state text,
	zip int,
	dollars text
);

create table types(
	typeid serial PRIMARY KEY,
	restaurantid int REFERENCES restaurants(restaurantid),
	type text
);

create table reviews(
	reviewid serial PRIMARY KEY,
	name text,
	zip int,
	reviewer text,
	rating int,
	review text,
	date date
);

grant all on restaurants, reviews, types to coremanager;
grant usage, select on all sequences in schema public to coremanager;