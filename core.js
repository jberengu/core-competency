const express= require('express');
var Pool= require('pg').Pool;
var bodyParser= require('body-parser');


const app= express();
var config= {
	host: 'localhost',
	user: 'coremanager',
	password: 'g~N]mD:>(9y2A,5S',
	database: 'core',
};

var pool= new Pool(config);

app.set('port', (8080));
app.use(bodyParser.json({type: 'application/json'}));
app.use(bodyParser.urlencoded({extended: true}));

app.post('/restaurant', async (req, res) => {
	var name= req.body.name;
	var city= req.body.city;
	var state= req.body.state;
	var zip= req.body.zip;
	var type= req.body.type;
	var dollars= req.body.dollars;

	if(!name || !city || !state || !zip || !type || !dollars){
			res.json({status: 'parameters not given'});
	}else{
		try{
			var check= await pool.query('select * from restaurants where name=$1 and zip= $2',[name,zip])
			if(check.rowCount < 1){
				var addrestaurant= await pool.query('insert into restaurants (name, city, state, zip, dollars) values ($1, $2, $3, $4, $5)', [name, city,state,zip,dollars]);
				var types= type.split(',');
				var getid= await pool.query('select restaurantid from restaurants where name=$1 and zip=$2',[name,zip]);
				var id= getid.rows[0].restaurantid;
				for(var i=0; i< types.length; i++){
					var addtype= await pool.query('insert into types (restaurantid, type) values ($1, $2)',[id, types[i]]);
				}
				res.json({status: 'OK'});
			}else{
				res.json({status: 'Restaurant already exists'});
			}
		} catch(e){
			console.error('Error running query '+ e);
			res.json({status: 'error running query'});
		}
	}
});

app.post('/review', async (req, res) => {
	var name= req.body.name;
	var zip= req.body.zip;
	var reviewer= req.body.reviewer;
	var rating= req.body.rating;
	var review= req.body.review;

	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
	var dateTime = date+' '+time;

	if(!name || !zip || !reviewer || !rating || !review){
			res.json({status: 'parameters not given'});
	}else{
		try{
			var check= await pool.query('select * from restaurants where name=$1 and zip= $2',[name,zip])
			if(check.rowCount >0){
				var addreview= await pool.query('insert into reviews (name, zip, reviewer, rating, review, date) values ($1, $2, $3, $4, $5, $6)', [name,zip,reviewer,rating, review, date])
				res.json({status: 'OK'});
			}else{
				res.json({status: 'restaurant does not exist'});
			}
		} catch(e){
			console.error('Error running query '+ e);
			res.json({status: 'error running query'});
		}
	}
});

app.get('/restaurant', async (req, res) => {
	var name= req.query.name;
	var zip= req.query.zip;

	if(!name || !zip){
			res.json({status: 'parameters not given'});
	}else{
		try{
			var response= await pool.query('select name, city, state, zip, dollars from restaurants where name=$1 and zip=$2',[name, zip])
			var getid= await pool.query('select restaurantid from restaurants where name=$1 and zip=$2',[name,zip])
			var getstars= await pool.query('select avg(rating) as stars from reviews where name=$1 and zip=$2',[name,zip])
			var getreviews= await pool.query('select count(rating) as reviews from reviews where name=$1 and zip=$2',[name, zip])
			var id= getid.rows[0].restaurantid;
			var gettypes= await pool.query('select type from types where restaurantid=$1',[id])
			var types= JSON.stringify(gettypes.rows.map(function (item){
				return item.type;
			})).replace(/"/g,'').replace('[','').replace(']','');
			var stars= getstars.rows[0].stars;
			var reviews= getreviews.rows[0].reviews;
			if(response.rowCount > 0){
				res.json({status: 'OK', result: {name: response.rows[0].name, city: response.rows[0].city, state: response.rows[0].state, zip: response.rows[0].zip, dollars: response.rows[0].dollars, type: types, stars: stars, reviews: reviews}});
			}else{
				res.json({status: 'restaurant does not exist'});
			}
		} catch(e){
			console.error('Error running query '+ e);
			res.json({status: 'error running query'});
		}
	}
});

app.get('/review', async (req, res) => {
	var name= req.query.name;
	var zip= req.query.zip;

	if(!name || !zip){
			res.json({status: 'parameters not given'});
	}else{
		try{
			var response= await pool.query('select reviewer, date, rating, review from reviews where name=$1 and zip=$2',[name, zip])
			if(response.rowCount>0){
				res.json({status: 'OK', result: response.rows.map(function(item){
					return {reviewer: item.reviewer, review_date: item.date, stars: item.rating, review: item.review};
				})});
			}else{
				res.json({status:'no reviews'});
			}
		} catch(e){
			console.error('Error running query '+ e);
			res.json({status: 'error running query'});
		}
	}
});

app.get('/find', async (req, res) => {
	var type= req.query.type;

	if(!type){
			res.json({status: 'parameters not given'});
	}else{
		try{
			var restaurantids= await pool.query('select restaurantid from types where type=$1',[type])
			if(restaurantids.rowCount > 0){
				var response= await pool.query('select r.name as restaurant, avg(rv.rating) as rating, count(rv.rating)as reviews, r.dollars,r.city, r.state, r.zip from restaurants as r join reviews as rv on r.name=rv.name and r.zip=rv.zip join types as t on r.restaurantid=t.restaurantid where type=$1 group by r.name,r.dollars,r.city,r.state,r.zip order by rating DESC',[type])
				res.json({status: 'OK', result: response.rows.map(function (item){
					return {restaurant: item.restaurant, rating: item.rating, reviews: item.reviews, dollars: item.dollars, city: item.city, state: item.state, zip: item.zip};
				})});
			}else{
				res.json({status: 'no restaurants with that type'});
			}
		} catch(e){
			console.error('Error running query '+ e);
			res.json({status: 'error running query'});
		}
	}
});

app.listen(app.get('port'), () => {
	console.log('Running');
})
















